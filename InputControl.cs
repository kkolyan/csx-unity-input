using System;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace Kk.CsxUnityInput
{
    public class InputControl
    {
        private readonly Func<Keyboard, KeyControl> _key;
        private readonly Func<Gamepad, ButtonControl> _button;
        private readonly Func<Gamepad, ButtonControl> _button2;
        private readonly Func<Mouse, ButtonControl> _mouse;
        

        public InputControl(Func<Mouse, ButtonControl> mouse)
        {
            _mouse = mouse;
        }

        public InputControl(Func<Mouse, ButtonControl> mouse, Func<Gamepad, ButtonControl> button)
        {
            _mouse = mouse;
            _button = button;
        }

        public InputControl(Func<Keyboard, KeyControl> key, Func<Gamepad, ButtonControl> button)
        {
            _key = key;
            _button = button;
        }

        public InputControl(Func<Keyboard, KeyControl> key, Func<Gamepad, ButtonControl> button, Func<Gamepad, ButtonControl> button2)
        {
            _key = key;
            _button = button;
            _button2 = button2;
        }

        public InputControl(Func<Keyboard, KeyControl> key)
        {
            _key = key;
        }

        public bool WasJustPressed()
        {
            var keyboard = Keyboard.current;
            if (keyboard != null && _key?.Invoke(keyboard)?.wasPressedThisFrame == true)
            {
                return true;
            }
            
            var mouse = Mouse.current;
            if (mouse != null && _mouse?.Invoke(mouse)?.wasPressedThisFrame == true)
            {
                return true;
            }

            var gamepad = Gamepad.current;
            return gamepad != null && (_button?.Invoke(gamepad)?.wasPressedThisFrame == true || _button2?.Invoke(gamepad)?.wasPressedThisFrame == true);
        }

        public bool WasJustReleased()
        {
            var keyboard = Keyboard.current;
            if (keyboard != null && _key?.Invoke(keyboard)?.wasReleasedThisFrame == true)
            {
                return true;
            }
            
            var mouse = Mouse.current;
            if (mouse != null && _mouse?.Invoke(mouse)?.wasReleasedThisFrame == true)
            {
                return true;
            }

            var gamepad = Gamepad.current;
            return gamepad != null && (_button?.Invoke(gamepad)?.wasReleasedThisFrame == true || _button2?.Invoke(gamepad)?.wasReleasedThisFrame == true);
        }

        public bool IsPressedNow()
        {
            var keyboard = Keyboard.current;
            if (keyboard != null && _key?.Invoke(keyboard)?.isPressed == true)
            {
                return true;
            }
            
            var mouse = Mouse.current;
            if (mouse != null && _mouse?.Invoke(mouse)?.isPressed == true)
            {
                return true;
            }

            var gamepad = Gamepad.current;
            return gamepad != null && (_button?.Invoke(gamepad)?.isPressed == true || _button2?.Invoke(gamepad)?.isPressed == true);
        }
    }
}